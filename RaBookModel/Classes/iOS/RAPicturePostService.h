//
//  RAPictureGetService.h
//  RaBook
//
//  Created by Leandro Fantin on 10/4/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RAService.h"

typedef void (^RAPicturePostServiceSuccessBlock)(NSURL *url);
typedef void (^RAPicturePostServiceCancelBlock)(void);
typedef void (^RAPicturePostServiceErrorBlock)(NSError *error);

@interface RAPicturePostService : RAService

- (instancetype)initWithPictureURL:(NSURL *)url successBlock:(RAPicturePostServiceSuccessBlock)successBlock cancelBlock:(RAPicturePostServiceCancelBlock)cancelBlock errorBlock:(RAPicturePostServiceErrorBlock)errorBlock;

- (void)start;
- (void)cancel;

@end
