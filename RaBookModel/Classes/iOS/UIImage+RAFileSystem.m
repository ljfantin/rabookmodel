//
//  UIImage+RAFileSystem.m
//  RaBook
//
//  Created by Leandro Fantin on 19/3/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "UIImage+RAFileSystem.h"

@implementation UIImage (RAFileSystem)

- (void)ml_saveToTempFileWithCompletionBlock:(void (^)(NSURL *url))completionBlock andErrorBlock:(void (^)(NSError *error))errorBlock
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSData *imageData = UIImageJPEGRepresentation(self, 1.0);
        NSURL *fileURL = nil;
        NSError *error = nil;
        
        NSString *fileName = [NSString stringWithFormat:@"%@.jpg", [[NSProcessInfo processInfo] globallyUniqueString]];
        fileURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:fileName]];
        [imageData writeToURL:fileURL
                      options:NSDataWritingFileProtectionNone
                        error:&error];
        imageData = nil;
        
        if (error) {
            if (errorBlock != nil)	{
                errorBlock(error);
            }
        } else {
            if (completionBlock != nil) {
                completionBlock(fileURL);
            }
        }
    });
}

@end
