//
//  UIImage+RAFileSystem.h
//  RaBook
//
//  Created by Leandro Fantin on 19/3/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (RAFileSystem)

- (void)ml_saveToTempFileWithCompletionBlock:(void (^)(NSURL *url))completionBlock andErrorBlock:(void (^)(NSError *error))errorBlock;

@end
