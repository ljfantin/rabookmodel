//
//  RAPictureGetService.m
//  RaBook
//
//  Created by Leandro Fantin on 10/4/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RAPicturePostService.h"
#import "RAModelUtils.h"
#import <AWSS3/AWSS3TransferManager.h>

NSString *const S3BucketName = @"pruebalea";
static NSString *const kRAPicturePostServiceParameters = @"book/picture";

@interface RAPicturePostService ()

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, copy) RAPicturePostServiceSuccessBlock successBlock;
@property (nonatomic, copy) RAPicturePostServiceCancelBlock cancelBlock;
@property (nonatomic, copy) RAPicturePostServiceErrorBlock errorBlock;
@property (nonatomic, strong) NSURLSessionDataTask * task;

@end

@implementation RAPicturePostService

- (instancetype)initWithPictureURL:(NSURL *)url successBlock:(RAPicturePostServiceSuccessBlock)successBlock cancelBlock:(RAPicturePostServiceCancelBlock)cancelBlock errorBlock:(RAPicturePostServiceErrorBlock)errorBlock
{
    self = [super init];
    if (self) {
        self.url = url;
        self.successBlock = successBlock;
        self.cancelBlock = cancelBlock;
        self.errorBlock = errorBlock;
    }
    return self;
}

- (NSString *)paramters
{
    return kRAPicturePostServiceParameters;
}

- (void)start
{
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.body = self.url;
    uploadRequest.key = [self.url lastPathComponent];
    uploadRequest.bucket = S3BucketName;
    uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    __weak typeof(self) weakSelf = self;
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {

        if (task.error) {
            if (weakSelf.errorBlock) {
                weakSelf.errorBlock(task.error);
            }
        }
        
        if (task.result) {
            NSURL *url = [self URLWithBucket:S3BucketName filename:uploadRequest.key];
            if (weakSelf.successBlock) {
                weakSelf.successBlock(url);
            }
        }

        return nil;
    }];
}

- (void)cancel
{
    [self.task cancel];
}

- (NSURL *)URLWithBucket:(NSString *)bucket filename:(NSString *)filename
{//https://<YourS3Endpoint>/<YourBucketName>/<YourObjectKeyName>
    NSString *stringWithURL = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@",bucket,filename];
    return [NSURL URLWithString:stringWithURL];
}


@end
