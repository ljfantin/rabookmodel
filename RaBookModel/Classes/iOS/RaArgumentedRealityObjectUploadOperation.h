//
//  RaArgumentedRealityObjectUploadOperation.h
//  RaBook
//
//  Created by Leandro Fantin on 19/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RAArgumentedRealityObjectModel.h"

typedef void (^RaArgumentedRealityObjectOperationUploadCompletionBlock)(void);
typedef void (^RaArgumentedRealityObjectOperationUploadCancelBlock)(void);
typedef void (^RaArgumentedRealityObjectOperationUploadErrorBlock)(NSError *error);

@interface RaArgumentedRealityObjectUploadOperation : NSOperation

- (instancetype)initWithScreenSize:(CGSize)size pictureURL:(NSURL *)pictureURL objectModel:(RAArgumentedRealityObjectModel *)raObjectModel completionBlock:(RaArgumentedRealityObjectOperationUploadCompletionBlock)completionBlock cancelBlock:(RaArgumentedRealityObjectOperationUploadCancelBlock)cancelBlock errorBlock:(RaArgumentedRealityObjectOperationUploadErrorBlock)errorBlock;

@end
