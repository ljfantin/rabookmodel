//
//  RAService.m
//  RaBook
//
//  Created by Leandro Fantin on 11/3/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RAService.h"

static NSString *const kRAService = @"https://murmuring-anchorage-91980.herokuapp.com/";
static NSString *const kDeviceScreen = @"%@x%@";


@interface RAService ()

@property (nonatomic, strong) AFHTTPSessionManager *manager;
@property (nonatomic, copy) NSString *host;

@end

@implementation RAService

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [self.manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"platform"];
        self.host = kRAService;
    }
    return self;
}


- (instancetype)initWithScreenSize:(CGSize)size
{
    self = [super init];
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        NSString *screenSizeHeader = [NSString stringWithFormat:kDeviceScreen, @(size.width).stringValue, @(size.height).stringValue];
        
        [self.manager.requestSerializer setValue:screenSizeHeader forHTTPHeaderField:@"screen_size"];
        [self.manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"platform"];
        self.host = kRAService;
    }
    return self;
}

- (NSString *)urlWithParameters
{
    NSString *parameters = [self paramters];
    NSMutableString *url = [[NSMutableString alloc] initWithString:kRAService];
    [url appendFormat:@"%@", parameters];
    return url;
}

- (NSString *)paramters
{
    return nil;
}

@end
