//
//  RAService.h
//  RaBook
//
//  Created by Leandro Fantin on 11/3/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface RAService : NSObject

@property (nonatomic, strong, readonly) AFHTTPSessionManager *manager;

- (instancetype)initWithScreenSize:(CGSize)size;
- (instancetype)init;

- (NSString *)urlWithParameters;

- (NSString *)paramters;

@end
