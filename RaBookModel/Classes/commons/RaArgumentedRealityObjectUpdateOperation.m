//
//  RaUpdatePictureOperation.m
//  RaBook
//
//  Created by Leandro Fantin on 19/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RaArgumentedRealityObjectUpdateOperation.h"
#import "RAGetService.h"

@interface RaArgumentedRealityObjectUpdateOperation ()

@property (nonatomic, assign) BOOL operationFinished;
@property (nonatomic, assign) BOOL operationExecuting;

@property (nonatomic, strong) RAGetService *service;
@property (nonatomic, copy) NSString *client;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, copy) RaArgumentedRealityObjectUpdateCompletionBlock completion;
@property (nonatomic, copy) RaArgumentedRealityObjectUpdateCancelBlock cancelBlock;
@property (nonatomic, copy) RaArgumentedRealityObjectUpdateErrorBlock errorBlock;

@end

@implementation RaArgumentedRealityObjectUpdateOperation

- (instancetype)initWithClientIdentifier:(NSString *)client size:(CGSize)size completionBlock:(RaArgumentedRealityObjectUpdateCompletionBlock)completionBlock cancelBlock:(RaArgumentedRealityObjectUpdateCancelBlock)cancelBlock errorBlock:(RaArgumentedRealityObjectUpdateErrorBlock)errorBlock
{
    self = [super init];
    if (self) {
        self.client = client;
        self.size = size;
        self.completion = completionBlock;
        self.cancelBlock = cancelBlock;
        self.errorBlock = errorBlock;
    }
    return self;
}

- (void)finishOperation
{
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    
    self.operationExecuting = NO;
    self.operationFinished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

#pragma mark - NSOperation

- (BOOL)isExecuting
{
    return self.operationExecuting;
}

- (BOOL)isFinished
{
    return self.operationFinished;
}

- (BOOL)isAsynchronous
{
    return YES;
}

- (void)start
{
    if ([self isCancelled]) {
        return;
    }
    __weak typeof(self) weakSelf = self;
    self.service = [[RAGetService alloc] initWithClientIdentifier:self.client screenSize:self.size successBlock:^(NSArray<RAArgumentedRealityObjectModel *> *objects) {
        if (weakSelf.completion) {
            weakSelf.completion(objects);
        }
        [weakSelf finishOperation];
    } cancelBlock:^{
        if (weakSelf.cancelBlock) {
            weakSelf.cancelBlock();
        }
        [weakSelf finishOperation];
    } errorBlock:^(NSError *error) {
        if (weakSelf.errorBlock) {
            weakSelf.errorBlock(error);
        }
        [self finishOperation];
    }];
    
    [self.service start];
}

- (void)cancel
{
    if (![self isCancelled]) {
        [super cancel];
        if ([self isExecuting]) {
            [self finishOperation];
            if (self.cancelBlock) {
                self.cancelBlock();
            }
        }
    }
}


@end
