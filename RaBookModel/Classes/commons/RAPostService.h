//
//  RAPostService.h
//  RaBook
//
//  Created by Leandro Fantin on 11/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RAArgumentedRealityObjectModel.h"
#import "RAService.h"

typedef void (^RAPostServiceSuccessBlock)(void);
typedef void (^RAPostServiceCancelBlock)(void);
typedef void (^RAPostServiceErrorBlock)(NSError *error);

@interface RAPostService : RAService

- (instancetype)initWithScreenSize:(CGSize)screenSize objectModel:(RAArgumentedRealityObjectModel *)object successBlock:(RAPostServiceSuccessBlock)successBlock cancelBlock:(RAPostServiceCancelBlock)cancelBlock errorBlock:(RAPostServiceErrorBlock)errorBlock;

- (void)start;
- (void)cancel;

@end
