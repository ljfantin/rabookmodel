 //
//  RAArgumentedRealityObjectModel.m
//  RaBook
//
//  Created by Leandro Fantin on 11/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RAArgumentedRealityObjectModel.h"
#import "RAModelUtils.h"

static NSString *const kIdKey = @"_id";
static NSString *const kClientIdKey = @"client_id";
static NSString *const kLayerIdentifierKey = @"layer_identifier";
static NSString *const kLayerTypeKey = @"layer_type";
static NSString *const kLayerTypeVimeoValue = @"vimeo";
static NSString *const kLayerTypeObjectValue = @"object";
static NSString *const kLayerTypeURLValue = @"url";
static NSString *const kSizeKey = @"screen_size";
static NSString *const kKeypointsKey = @"keypoints";
static NSString *const kDescriptorsKey = @"descriptors";
static NSString *const kPictureUrl = @"picture_url";
static NSString *const kPictureWidthKey = @"picture_width";
static NSString *const kPictureHeightKey = @"picture_height";

@implementation RAArgumentedRealityObjectModel

- (instancetype)initWithDictionary:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        if (![RAModelUtils isNullOrEmpty:json[kIdKey]]) {
            self.identifier = json[kIdKey];
        }
        
        if (![RAModelUtils isNullOrEmpty:json[kClientIdKey]]) {
            self.client = json[kClientIdKey];
        }
        
        if (![RAModelUtils isNullOrEmpty:json[kLayerIdentifierKey]]) {
            self.layerIdentifier = json[kLayerIdentifierKey];
        }
        
        if (![RAModelUtils isNullOrEmpty:json[kLayerTypeKey]]) {
            NSString *layerType = json[kLayerTypeKey];
            if ([layerType isEqualToString:kLayerTypeVimeoValue]) {
                self.layerType = RAArgumentedRealityObjectLayerTypeVimeo;
            }
            else
            if ([layerType isEqualToString:kLayerTypeObjectValue]) {
                self.layerType = RAArgumentedRealityObjectLayerTypeObject;
            }
            else {
                self.layerType = RAArgumentedRealityObjectLayerTypeURL;
            }
        }
        
        if (![RAModelUtils isNullOrEmpty:json[kPictureUrl]]) {
            self.pictureUrl = [NSURL URLWithString:json[kPictureUrl]];
        }
        
        if (![RAModelUtils isNullOrEmpty:json[kKeypointsKey]]) {
            NSArray *keypoints = json[kKeypointsKey];
            NSMutableArray<NSNumber *> *keypointsResult = [NSMutableArray array];
            for (NSNumber *point in keypoints) {
                [keypointsResult addObject:point];
            }
            self.keypoints = keypointsResult;
        }
        
        if (![RAModelUtils isNullOrEmpty:json[kDescriptorsKey]]) {
            NSArray *descriptors = json[kDescriptorsKey];
            NSMutableArray<NSNumber *> *descriptorsResult = [NSMutableArray array];
            for (NSNumber *point in descriptors) {
                [descriptorsResult addObject:point];
            }
            self.descriptors = descriptorsResult;
        }

        if (![RAModelUtils isNullOrEmpty:json[kPictureWidthKey]]) {
            NSNumber * number = json[kPictureWidthKey];
            self.pictureWidth = number.floatValue;
        }
        
        if (![RAModelUtils isNullOrEmpty:json[kPictureHeightKey]]) {
            NSNumber * number = json[kPictureHeightKey];
            self.pictureHeight = number.floatValue;
        }
        
    }
    return self;
}

- (instancetype)initWithIdentifier:(NSString *)identifier client:(NSString *)client size:(CGSize)size pictureUrl:(NSURL *)pictureUrl layerType:(RAArgumentedRealityObjectLayerType)layerType layerIdentifier:(NSString *)layerIdentifier pictureWidth:(CGFloat)pictureWidth pictureHeight:(CGFloat)pictureHeight keypoints:(NSArray<NSNumber *> *)keypoints descriptors:(NSArray<NSNumber *> *)descriptors
{
    self = [super init];
    if (self) {
        self.identifier = identifier;
        self.client = client;
        self.pictureUrl = pictureUrl;
        self.layerType = layerType;
        self.layerIdentifier = layerIdentifier;
        self.pictureUrl = pictureUrl;
        self.pictureWidth = pictureWidth;
        self.pictureHeight = pictureHeight;
        self.keypoints = keypoints;
        self.descriptors = descriptors;
    }
    return self;
}

- (NSString *)layerTypeJsonMapping
{
    NSString *result;
    if (self.layerType == RAArgumentedRealityObjectLayerTypeVimeo) {
        result = kLayerTypeVimeoValue;
    }
    else
    if (self.layerType == RAArgumentedRealityObjectLayerTypeObject) {
        result = kLayerTypeObjectValue;
    }
    else {
        result = kLayerTypeURLValue;
    }
    return result;
}

- (NSDictionary *)marshall;
{
    NSString *layerType = [self layerTypeJsonMapping];
    
    return @{kClientIdKey:self.client,kLayerTypeKey:layerType, kLayerIdentifierKey : self.layerIdentifier, kPictureWidthKey : @(self.pictureWidth), kPictureHeightKey: @(self.pictureHeight), kPictureUrl:self.pictureUrl.absoluteString, kKeypointsKey:self.keypoints, kDescriptorsKey:self.descriptors};
}

@end
