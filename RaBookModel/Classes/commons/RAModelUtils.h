//
//  RAModelUtils.h
//  RaBook
//
//  Created by Leandro Fantin on 11/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RAModelUtils : NSObject

+ (BOOL)isNullOrEmpty:(id)object;

@end
