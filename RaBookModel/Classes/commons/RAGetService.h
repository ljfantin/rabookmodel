//
//  RAGetService.h
//  RaBook
//
//  Created by Leandro Fantin on 7/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RAArgumentedRealityObjectModel.h"
#import "RAService.h"

typedef void (^RAGetServiceSuccessBlock)(NSArray<RAArgumentedRealityObjectModel *> *objects);
typedef void (^RAGetServiceCancelBlock)(void);
typedef void (^RAGetServiceErrorBlock)(NSError *error);

@interface RAGetService : RAService

- (instancetype)initWithClientIdentifier:(NSString *)identifier screenSize:(CGSize)screenSize successBlock:(RAGetServiceSuccessBlock)successBlock cancelBlock:(RAGetServiceCancelBlock)cancelBlock errorBlock:(RAGetServiceErrorBlock)errorBlock;

- (void)start;
- (void)cancel;

@end
