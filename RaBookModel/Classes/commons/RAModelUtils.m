//
//  RAModelUtils.m
//  RaBook
//
//  Created by Leandro Fantin on 11/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RAModelUtils.h"

@implementation RAModelUtils

+ (BOOL)isNullOrEmpty:(id)object {
    
    if (object == nil || [object isEqual:[NSNull null]]) {
        return YES;
    }
    
    if ([object isKindOfClass:[NSString class]]) {
        if ([[object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
            return YES;
        }
    }
    
    return NO;
}

@end
