//
//  RAArgumentedRealityObjectModel.h
//  RaBook
//
//  Created by Leandro Fantin on 11/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RAMarshallModel.h"

typedef NS_ENUM(NSInteger, RAArgumentedRealityObjectLayerType) {
    RAArgumentedRealityObjectLayerTypeVimeo,
    RAArgumentedRealityObjectLayerTypeObject,
    RAArgumentedRealityObjectLayerTypeURL
};

@interface RAArgumentedRealityObjectModel : NSObject <RAMarshallModel>

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *client;
@property (nonatomic, assign) RAArgumentedRealityObjectLayerType layerType;
@property (nonatomic, copy) NSString *layerIdentifier;
@property (nonatomic, strong) NSURL *pictureUrl;
@property (nonatomic, assign) CGFloat pictureWidth;
@property (nonatomic, assign) CGFloat pictureHeight;
@property (nonatomic, strong) NSArray<NSNumber *> *keypoints;
@property (nonatomic, strong) NSArray<NSNumber *> *descriptors;

- (instancetype)initWithDictionary:(NSDictionary *)json;

- (instancetype)initWithIdentifier:(NSString *)identifier client:(NSString *)client size:(CGSize)size pictureUrl:(NSURL *)pictureUrl layerType:(RAArgumentedRealityObjectLayerType)layerType layerIdentifier:(NSString *)layerIdentifier pictureWidth:(CGFloat)width pictureHeight:(CGFloat)height keypoints:(NSArray<NSNumber *> *)keypoints descriptors:(NSArray<NSNumber *> *)descriptors;

@end
