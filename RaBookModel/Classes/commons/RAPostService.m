//
//  RAPicturePostService.m
//  RaBook
//
//  Created by Leandro Fantin on 11/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RAPostService.h"
#import <AFNetworking/AFNetworking.h>

static NSString *const kRAPictureServiceParameters = @"book/%@/raobject/";

@interface RAPostService ()

@property (nonatomic, strong) RAArgumentedRealityObjectModel *objectModel;
@property (nonatomic, copy) RAPostServiceSuccessBlock successBlock;
@property (nonatomic, copy) RAPostServiceCancelBlock cancelBlock;
@property (nonatomic, copy) RAPostServiceErrorBlock errorBlock;
@property (nonatomic, strong) NSURLSessionDataTask * task;

@end

@implementation RAPostService

- (instancetype)initWithScreenSize:(CGSize)screenSize objectModel:(RAArgumentedRealityObjectModel *)object successBlock:(RAPostServiceSuccessBlock)successBlock cancelBlock:(RAPostServiceCancelBlock)cancelBlock errorBlock:(RAPostServiceErrorBlock)errorBlock
{
    self = [super initWithScreenSize:screenSize];
    if (self) {
        self.objectModel = object;
        self.successBlock = successBlock;
        self.cancelBlock = cancelBlock;
        self.errorBlock = errorBlock;
    }
    return self;
}

- (NSString *)paramters
{
    return [NSString stringWithFormat:kRAPictureServiceParameters, self.objectModel.client];
}

- (void)start
{
    __weak typeof(self) weakSelf = self;
    NSDictionary *json = [self.objectModel marshall];
    NSString *url = [self urlWithParameters];
    self.task = [self.manager POST:url parameters:json progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (weakSelf.successBlock) {
            weakSelf.successBlock();
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (weakSelf.errorBlock) {
            weakSelf.errorBlock(error);
        }
    }];
}

- (void)cancel
{
    [self.task cancel];
}


@end
