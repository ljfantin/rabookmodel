//
//  RAGetService.m
//  RaBook
//
//  Created by Leandro Fantin on 7/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RAGetService.h"
#import <AFNetworking/AFNetworking.h>
#import "RAModelUtils.h"
#import "RAArgumentedRealityObjectModel.h"


static NSString *const kRAGetServiceParameters = @"book/%@/raobject/";

@interface RAGetService ()

@property (nonatomic, copy) RAGetServiceSuccessBlock successBlock;
@property (nonatomic, copy) RAGetServiceCancelBlock cancelBlock;
@property (nonatomic, copy) RAGetServiceErrorBlock errorBlock;
@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, strong) NSURLSessionDataTask * task;

@end

@implementation RAGetService

- (instancetype)initWithClientIdentifier:(NSString *)identifier screenSize:(CGSize)screenSize successBlock:(RAGetServiceSuccessBlock)successBlock cancelBlock:(RAGetServiceCancelBlock)cancelBlock errorBlock:(RAGetServiceErrorBlock)errorBlock

{
    self = [super initWithScreenSize:screenSize];
    if (self) {
        self.identifier = identifier;
        self.successBlock = successBlock;
        self.cancelBlock = cancelBlock;
        self.errorBlock = errorBlock;
    }
    return self;
}

- (NSString *)paramters
{
    return [NSString stringWithFormat:kRAGetServiceParameters, self.identifier];
}

- (void)start
{
    __weak typeof(self) weakSelf = self;
    NSString *url = [self urlWithParameters];
    self.task = [self.manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray<RAArgumentedRealityObjectModel *> *pictures = [NSMutableArray array];
        if (responseObject) {
            NSArray *responseArray = responseObject;
            for (NSDictionary * jsonWithPicture in responseArray) {
                RAArgumentedRealityObjectModel *picture = [[RAArgumentedRealityObjectModel alloc] initWithDictionary:jsonWithPicture];
                [pictures addObject:picture];
            }
        }
        weakSelf.successBlock(pictures);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        weakSelf.errorBlock(error);
    }];
}

- (void)cancel
{
    [self.task cancel];
}

@end
