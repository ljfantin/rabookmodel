//
//  RaUpdatePictureOperation.h
//  RaBook
//
//  Created by Leandro Fantin on 19/2/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RAArgumentedRealityObjectModel.h"

typedef void (^RaArgumentedRealityObjectUpdateCompletionBlock)(NSArray<RAArgumentedRealityObjectModel *> *objects);
typedef void (^RaArgumentedRealityObjectUpdateCancelBlock)(void);
typedef void (^RaArgumentedRealityObjectUpdateErrorBlock)(NSError *error);

@interface RaArgumentedRealityObjectUpdateOperation : NSOperation

- (instancetype)initWithClientIdentifier:(NSString *)client size:(CGSize)size completionBlock:(RaArgumentedRealityObjectUpdateCompletionBlock)completionBlock cancelBlock:(RaArgumentedRealityObjectUpdateCancelBlock)cancelBlock errorBlock:(RaArgumentedRealityObjectUpdateErrorBlock)errorBlock;

@end
