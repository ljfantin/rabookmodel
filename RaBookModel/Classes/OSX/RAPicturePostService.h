//
//  RAPicturePostService.h
//  Pods
//
//  Created by Leandro Fantin on 2/5/17.
//
//

#import <Foundation/Foundation.h>
#import "RAService.h"

typedef void (^RAPicturePostServiceSuccessBlock)(NSString *url);
typedef void (^RAPicturePostServiceCancelBlock)(void);
typedef void (^RAPicturePostServiceErrorBlock)(NSError *error);

@interface RAPicturePostService : RAService

- (instancetype)initWithImage:(NSImage *)image successBlock:(RAPicturePostServiceSuccessBlock)successBlock cancelBlock:(RAPicturePostServiceCancelBlock)cancelBlock errorBlock:(RAPicturePostServiceErrorBlock)errorBlock;

- (void)start;
- (void)cancel;

@end
