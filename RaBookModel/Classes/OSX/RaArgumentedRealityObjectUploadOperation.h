//
//  RaArgumentedRealityObjectUploadOperation.h
//  Pods
//
//  Created by Leandro Fantin on 3/5/17.
//
//

#import <Foundation/Foundation.h>
#import "RAArgumentedRealityObjectModel.h"

typedef void (^RaArgumentedRealityObjectOperationUploadCompletionBlock)(void);
typedef void (^RaArgumentedRealityObjectOperationUploadCancelBlock)(void);
typedef void (^RaArgumentedRealityObjectOperationUploadErrorBlock)(NSError *error);

@interface RaArgumentedRealityObjectUploadOperation : NSOperation

- (instancetype)initWithScreenSize:(CGSize)size objectModel:(RAArgumentedRealityObjectModel *)raObjectModel completionBlock:(RaArgumentedRealityObjectOperationUploadCompletionBlock)completionBlock cancelBlock:(RaArgumentedRealityObjectOperationUploadCancelBlock)cancelBlock errorBlock:(RaArgumentedRealityObjectOperationUploadErrorBlock)errorBlock;

@end
