//
//  RAPictureGetService.m
//  RaBook
//
//  Created by Leandro Fantin on 10/4/17.
//  Copyright © 2017 com.piantao. All rights reserved.
//

#import "RAPicturePostService.h"
#import "RAModelUtils.h"

static NSString *const kRAPicturePostServiceParameters = @"book/picture";

@interface RAPicturePostService ()

@property (nonatomic, strong) NSImage *image;
@property (nonatomic, copy) RAPicturePostServiceSuccessBlock successBlock;
@property (nonatomic, copy) RAPicturePostServiceCancelBlock cancelBlock;
@property (nonatomic, copy) RAPicturePostServiceErrorBlock errorBlock;
@property (nonatomic, strong) NSURLSessionDataTask * task;

@end

@implementation RAPicturePostService

- (instancetype)initWithImage:(NSImage *)image successBlock:(RAPicturePostServiceSuccessBlock)successBlock cancelBlock:(RAPicturePostServiceCancelBlock)cancelBlock errorBlock:(RAPicturePostServiceErrorBlock)errorBlock
{
    self = [super init];
    if (self) {
        self.image = image;
        self.successBlock = successBlock;
        self.cancelBlock = cancelBlock;
        self.errorBlock = errorBlock;
    }
    return self;
}

- (NSString *)paramters
{
    return kRAPicturePostServiceParameters;
}

- (void)start
{
    __weak typeof(self) weakSelf = self;
    NSString *url = [self urlWithParameters];
    
    CGImageRef cgImageRef = [self.image CGImageForProposedRect:nil context:nil hints:nil];
    NSBitmapImageRep *bmp = [[NSBitmapImageRep alloc] initWithCGImage:cgImageRef];
    NSData *data = [bmp representationUsingType:NSBitmapImageFileTypeJPEG properties:nil];
    
    self.task = [self.manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:data name:@"file" fileName:@"file.jpg" mimeType:@"image/jpeg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Uploading...");
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSString *pictureUrl;
        if (![RAModelUtils isNullOrEmpty:responseObject[@"pictureUrl"]]) {
            pictureUrl = responseObject[@"pictureUrl"];
        }
        if (weakSelf.successBlock) {
            weakSelf.successBlock(pictureUrl);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (weakSelf.errorBlock) {
            weakSelf.errorBlock(error);
        }
    }];
    [self.task resume];
}

- (void)cancel
{
    [self.task cancel];
}

@end
