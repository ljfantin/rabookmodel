//
//  RaArgumentedRealityObjectUploadOperation.m
//  Pods
//
//  Created by Leandro Fantin on 3/5/17.
//
//

#import "RaArgumentedRealityObjectUploadOperation.h"

#import "RaArgumentedRealityObjectUploadOperation.h"
#import "RAPostService.h"
#import "RAPicturePostService.h"

@interface RaArgumentedRealityObjectUploadOperation ()

@property (nonatomic, assign) BOOL operationFinished;
@property (nonatomic, assign) BOOL operationExecuting;

@property (nonatomic, strong) RAPostService *service;
@property (nonatomic, strong) RAPicturePostService *pictureService;

@property (nonatomic, assign) CGSize screenSize;
@property (nonatomic, strong) RAArgumentedRealityObjectModel *raObjectModel;
@property (nonatomic, copy) RaArgumentedRealityObjectOperationUploadCompletionBlock completion;
@property (nonatomic, copy) RaArgumentedRealityObjectOperationUploadCancelBlock cancelBlock;
@property (nonatomic, copy) RaArgumentedRealityObjectOperationUploadErrorBlock errorBlock;

@end

@implementation RaArgumentedRealityObjectUploadOperation

- (instancetype)initWithScreenSize:(CGSize)size objectModel:(RAArgumentedRealityObjectModel *)raObjectModel completionBlock:(RaArgumentedRealityObjectOperationUploadCompletionBlock)completionBlock cancelBlock:(RaArgumentedRealityObjectOperationUploadCancelBlock)cancelBlock errorBlock:(RaArgumentedRealityObjectOperationUploadErrorBlock)errorBlock
{
    self = [super init];
    if (self) {
        self.screenSize = size;
        self.raObjectModel = raObjectModel;
        self.completion = completionBlock;
        self.cancelBlock = cancelBlock;
        self.errorBlock = errorBlock;
    }
    return self;
}

- (void)finishOperation
{
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    
    self.operationExecuting = NO;
    self.operationFinished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

#pragma mark - NSOperation

- (BOOL)isExecuting
{
    return self.operationExecuting;
}

- (BOOL)isFinished
{
    return self.operationFinished;
}

- (BOOL)isAsynchronous
{
    return YES;
}

- (void)start
{
    if ([self isCancelled]) {
        return;
    }
    __weak typeof(self) weakSelf = self;
    self.service = [[RAPostService alloc] initWithScreenSize:self.screenSize objectModel:self.raObjectModel successBlock:^{
        if (weakSelf.completion) {
            weakSelf.completion();
        }
        [weakSelf finishOperation];
    } cancelBlock:^{
        if (weakSelf.cancelBlock) {
            weakSelf.cancelBlock();
        }
        [weakSelf finishOperation];
    } errorBlock:^(NSError *error) {
        if (weakSelf.errorBlock) {
            weakSelf.errorBlock(error);
        }
        [self finishOperation];
    }];
    
    [self.service start];
}

- (void)cancel
{
    if (![self isCancelled]) {
        [super cancel];
        if ([self isExecuting]) {
            [self finishOperation];
            if (self.cancelBlock) {
                self.cancelBlock();
            }
        }
    }
}


@end
