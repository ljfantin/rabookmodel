# RaBookModel

[![CI Status](http://img.shields.io/travis/ljfantin/RaBookModel.svg?style=flat)](https://travis-ci.org/ljfantin/RaBookModel)
[![Version](https://img.shields.io/cocoapods/v/RaBookModel.svg?style=flat)](http://cocoapods.org/pods/RaBookModel)
[![License](https://img.shields.io/cocoapods/l/RaBookModel.svg?style=flat)](http://cocoapods.org/pods/RaBookModel)
[![Platform](https://img.shields.io/cocoapods/p/RaBookModel.svg?style=flat)](http://cocoapods.org/pods/RaBookModel)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RaBookModel is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RaBookModel"
```
pod repo push personal_specs RaBookModel.podspec --allow-warnings --use-libraries --skip-import-validation

## Author

ljfantin, ljfantin@gmail.com

## License

RaBookModel is available under the MIT license. See the LICENSE file for more info.
