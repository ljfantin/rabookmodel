#
# Be sure to run `pod lib lint RaBookModel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RaBookModel'
  s.version          = '0.1.6'
  s.summary          = 'A short description of RaBookModel.'
  s.description      = 'descripcion'
  s.homepage         = 'https://ljfantin@bitbucket.org/ljfantin/rabookmodel'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ljfantin' => 'lfantin@gmail.com' }
  s.source           = { :git => 'https://ljfantin@bitbucket.org/ljfantin/rabookmodel.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.9'

  s.source_files = 'RaBookModel/Classes/commons/**/*'
  s.ios.source_files = 'RaBookModel/Classes/iOS/**/*'
  s.osx.source_files = 'RaBookModel/Classes/OSX/**/*'
  
  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.ios.frameworks = 'UIKit','Foundation'
  
  s.dependency 'AFNetworking', '~> 3.1'
  s.ios.dependency 'AWSS3'
end
